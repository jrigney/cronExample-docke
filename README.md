# cronExample-docker
An example of running a cron job in a docker container.

## build image with

```
docker build --rm -t jrigney/cron-example .
```

## run it

```
docker run -t -i jrigney/cron-example
```

